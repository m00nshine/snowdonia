##
# **Articles of Association**

##
# **Companies Act 2006 Private Company Limited by Guarantee**

##
# **Astralship Collective CIC**

##
# **Section One**

##
# **Interpretations &amp; Definitions**

1. In these Articles:

&quot; **Address&quot;** means a postal address or, for the purposes of electronic communication, a fax number, email address or telephone number for receiving text messages;

&quot; **Articles&quot;** means the Co-operative&#39;s articles of association;

&quot; **The Board of Directors&quot;** or &quot; **Board&quot;** means all those persons appointed to perform the duties of directors of the Company;

&quot; **Companies Acts&quot;** or **&quot;the Act&quot;** means the Companies Acts (as defined in section 2 of the Companies Act 2006) in so far as they apply to the company;

&quot; **The Co-operative&quot;** means the above-named company;

&quot; **Co-operative Principles&#39;&#39;** are the principles defined in the International Co-operative Alliance Statement of Cooperative Identity. These principles as understood and interpreted by the Astralship consist of the following fundamentals:

1. voluntary and open membership,
2. democratic member control,
3. member economic participation, autonomy and independence,
4. education, training and information,
5. cooperation among cooperatives
6. concern for the community;

&quot; **Co-operative Values&quot;** refers to the following values:

1. self help,
2. self responsibility,
3. democracy,
4. equality,
5. equity and solidarity and the Co-operative Principles;

&quot; **Director&quot;** means a director of the Co-operative and includes any person occupying the position of Director, by whatever name called;

&quot; **Document&quot;** includes, unless otherwise stated, any document sent or supplied in electronic form;

&quot; **Electronic means**&quot; has the meaning given in section 1168 of the Companies Act 2006;

&quot; **Employee&quot;** means anyone over the age of 16 holding a contract of employment with the Cooperative to perform at least eight hours of work per week for the Cooperative;

&quot; **Entrenched&quot;** has the meaning given by section 22 of the Companies Act 2006 and as detailed under the heading &#39;resolutions&#39; in these Articles;

&quot; **Member** has the same meaning given in section 112 of the Companies Act 2006 to &#39;&#39;Member&#39;&#39;, as detailed under &#39;Membership&#39; in these Articles;

&quot; **Regulations&quot;** has the meaning as detailed under &#39;Regulations&#39; in these Articles;

&quot; **Secretary&quot;** means any person appointed to perform the duties of the Secretary of the Cooperative;

&quot; **Writing&quot;** means the representation or reproduction of words, symbols or other information in a visible form by any method or combination of methods, whether sent or supplied in electronic form or otherwise.

1. Unless the context requires otherwise, other words or expressions contained in these Articles bear the same meaning as in the Companies Act 2006 as in force on the date when these Articles become binding on the Co-operative. Schedule 1 to the Companies (Model Articles) Regulations 2008 shall apply to the Cooperative, save where amended or replaced by these Articles. In the case of any variation or inconsistency between these Articles and the model articles, these Articles shall prevail.

##


##
# **Section Two**

##
# **Purpose &amp; Objects**

2. The name of the Company is: Astralship Collective CIC.

3. The registered office of the Company is at:

4. The company is to be a Community Interest Company. Its objects are to carry on any industry, business or trade in accordance with co-operative principles in order to establish novel facilitated, residential co-living co-work spaces where members will be able to live some or all of the time, having their essential needs met and be able to apply their talents to creating real value for humanity in an optimised way. With an eye to considering and developing solutions to grand societal, technological and environmental challenges. Wherever the above mentioned spaces are established the CIC will seek to directly or indirectly support local residents, whether or not they are CIC members, through local activities in particular by carrying out projects that support local community education and wellbeing.

# **Section Three**

# **Membership**

5. The subscribers to the Memorandum are the first members of the Company. Such other persons as are admitted to membership in accordance with the Articles shall be members of the Company. They will be drawn from one or more categories of stakeholder, corresponding to the different types of involvement those stakeholders have with the co-operative.

6. The Co-operative may admit to membership any self-employed contractor or individual, corporate body or nominee of an unincorporated body, firm or partnership that wishes to use the services of the Co-operative and has paid or agreed to pay any subscription or other sum due in respect of membership or the use of the Co-operative&#39;s services and meets the requirements of membership provided that the criteria for membership are applied equally to all applicants.

**7. Employee Members:** All Employees on taking up employment with the Co­operative may be admitted to membership of the Cooperative as an Employee Member, except in the situation that the membership of the Cooperative though a general meeting, by a majority vote decided to exclude that Employee from membership on the following specific grounds :

a) The Employee is at the time of the general meeting newly appointed and under a reasonable probationary period as may be specified in their terms and conditions of employment contract or agreement.

b) The Employee is at the time of the general meeting working less than a prescribed number of hours per week (or per month); provided that any such criteria for exclusion is applied equally to all Employees and formally detailed &amp; ratified by the cooperative in a company bylaw.

c) In accordance with the Cooperative Principle of voluntary and open membership, whilst the Cooperative may encourage Employees under it to become Members, no employee may be forced as a condition or prerequisite of employment to become a member.

**8. Service User Members:** User members, who are the main beneficiaries of the Company&#39;s services, shall play a principal role in the governance and orientation of the Company. User members shall benefit principally from those services and not from investments.

**9. Applications for membership:**

a) Every person who wishes to become a member shall deliver to the company an application for membership in such form (and containing such information) as the Directors require, executed by them. The Company is open to applications for membership in the appropriate class without discrimination, subject to and limited by the cooperatives membership policies as agreed to and formalised by the members of the cooperative though resolutions passed though the procedures laid out in these articles. This policy will be made available to current and prospective members, and will specify:

i. the responsibilities of membership that members are expected to comply with,

ii. the application procedure, which may include a reasonable probationary period

iii. the minimum investment for membership in each class, which for user members must be set at no more than £50 payable in the first year of membership.

iv. the transactions with the Company that qualify an applicant for membership in each class.

b) All members of the cooperation must be aged sixteen or over.

c) The board shall have the right to refuse membership to an individual where it believes there is good reason to do so. No person shall be admitted a member of the Company unless they are approved by the Directors.

c) Membership is not transferable to anyone else.

**10. Cancellation of Membership:**

a) Membership of the Company will be cancelled under the following circumstances:

i. at the discretion of the board, if the member is unable to provide evidence that

they conduct (or intend to conduct within a reasonable timeframe) the

transactions with the Company (referred to in rule 5. above) that originally

qualified them for membership;

ii. on receipt of a written request by a member for the cancellation of their own

membership;

iii. if, over a period of two years, reasonable attempts to communicate with the

member (including a written warning that membership may be cancelled if no

response is received) elicit no response;

iv. if the member is expelled according to the procedure, as detailed below.

v. if the member dies or ceases to exist;

vi. or otherwise according to the Articles.

Vii. if a member (employee member) ceases to be employed by the cooperative.

1. A member of the cooperative may be expelled and excised from the membership of the cooperative though a resolution made at a general meeting that calls for such. A resolution to expel a member may only pass if:

I. the resolution acquires the support of 75% of the members present for that general meeting.

ii. The Member has been given at least five days&#39; notice in Writing of the general meeting at which the resolution to expel them will be proposed and the reasons for its proposition.

Iii. The Member or, at the option of the Member, an individual who is there to represent them (who need not be a Member of the Co-operative) has been allowed to make representations to the general meeting.

1. Members who are facing a resolution of expulsion at a general meeting may not vote on the resolution itself, nor may any relatives of there&#39;s who happen to be members of the cooperative.

1. Following a resolution for expulsion, the expelled member may not be readmitted to any class of membership without a further resolution at a general meeting;

1. If a member of the cooperative is expelled from its membership. The member in question must within a day of the resolution being passed be given written notice of the decision of the cooperative, and sufficient time (not exceeding two weeks nor less then five days) to prepare an appeal to a general meeting should they wish to challenge the decision.

1. An expelled member may at the next general meeting of the cooperative appeal against a decision to expel them. A resolution to overturn a passed resolution to expel the member may only pass if;

i. it acquires 75% support of the members present.

ii. is not vetoed by any board or commonwealth council member present at the general meeting..

g) Members may in accordance with the law nominate an individual or individuals

to whom the full value of their property in the Company should be transferred i

the event of the Company receiving proof of the member&#39;s death. If there is no

nominee, then all property held by a deceased member shall be transferred to

their personal representative upon request. The Company will also transfer

property in the Company held by a bankrupt member to their trustee in

bankruptcy upon receipt of a valid claim.

11. **Register of members:**

1. A register of members is kept at the registered office, and will include:

i. the name of every member;

ii. the address and other contact details, and whether electronic communications

are to be used;

iii. The class of membership for each member;

iv. any loans or other property held by members;

v. the date on which the member&#39;s name was entered on the register, and the

date on which they ceased to be members;

vi. the names of directors and officers of the Company, the positions held by

them, and the dates on which those appointments began and ended.

a) While a member shall be allowed on reasonable notice to inspect extracts

from the register, names in (i) and financial data in (iv) will only be linked to other

data in accordance with privacy law and best practice.

# **Section Four**

# **Governance**

12. The Cooperative in so far as it possesses a general overall philosophy in relation to its principles of governance is dedicated to involving, consulting with and empowering its membership. So that the direction of the cooperative, the needs of its members and the manner in which it can benefit the community is determined by the wishes of its members.

13. The foundational sovereign body of the Company is the general meeting, which shall appoint from within the membership, a board of directors to manage the day to day operations of the Company, and may, should it believe it necessary convene a Commonwealth Council to provide wider input and oversight in accordance with rule 43. (b).

14. Within each class of membership, voting at general meetings shall be on the basis of one member, one vote.

15. Where a member is an organisation (incorporated or unincorporated), its rights will be exercised by a person who is nominated by that organisation in accordance with their rules. That person will cease to do so if for any reason that nomination is no longer sustained by the member organisation.

16. The Company will have a secretary and a treasurer. These positions may be appointed or replaced though a resolution passed at a general meeting. If a general meeting does not appoint them, the board of directors will be duty-bound to do so.

**17. Standing Orders:**

a) Each instance of the general meeting and board may be governed by standing orders (that is, those policies and secondary rules that directly address the method by which the registered Articles are implemented). These may be adopted from time to time by the board or by a general meeting and will remain in force until they are amended or repealed by a general meeting (or by the board, providing that no general meeting has previously adopted or amended them). Such standing orders may not contradict these Articles on any matter, and are void insofar as they do so.

b) Standing orders may or may not provide for additional means by which members can raise concerns within the Company and contribute to resolving them, including but not limited to:

i. local or specialist groups of members both elected and appointed,

ii. consultative procedures and discussion forums open to some or all members

iii. opportunities within meetings for ordinary members present to contribute spontaneously

iv. appeal and investigatory procedures to address grievances, injustices and complaints

c) In order to ensuring expediency with regards to decisions, information sharing and conflict resolution, the standing orders should aim to ensure that when an issue has been raised at a board or general meeting by a voting member seeking a decision, that decision should take place within a short enough period of time that no otherwise available options are timed out. Subject to that constraint, standing orders may be adopted that require or enable consensus building and inclusive development of proposals prior to the point of decision, and these may or may not include:

i. procedures for testing consensus or measuring levels of agreement

ii. development and evaluation of alternative, contingent or qualified proposals

iii. wider consultation and impact assessments

iv. preparatory meetings with a range of stakeholders and/or disputants v. extended and structured discussion of proposals within meetings.

**18. General meetings**

a) Annual General Meetings will be convened by the board of directors within six months of the close of the Company&#39;s financial year, these meetings must include the following items on their agendas:

i. presentation of accounts and reports specified in 41. below

ii. appointment of auditors, or disapplication of audit requirements, as required by 47. (b) below

iii. election of a board of directors as required by 26. below

iv. ratification of any key decisions required by 39. (b) below, or by any standing orders relating to key decisions

b) The directors will also convene General Meetings:

i. at the request of the Commonwealth Council;

ii. at the request in writing of 5 or more members, provided that they amount to at least 15% of the membership (rounded up), or 30% of the membership in any one class (rounded up);

iii. in the event of the number of elected directors falling below three, or in the event that more than 25% of the directors are not core members;

iv. in the event that the board fails to secure the appointment of a secretary and a treasurer;

v. at any other time deemed appropriate by the board.

c) All members, and any auditor or accountants appointed by the company, will be notified in writing to their registered address or (where the member has supplied suitable contact details) by electronic communication, no less than fourteen days before the date of the meeting. The notice must inform members:

i. the reason for the meeting, being one of the causes listed in 18 (a) or (b) above

ii. the time and place of the meeting

iii. the means by which participation through a live electronic link will be enabled, if any

iv. how to propose resolutions

v. how to stand for election to the board (unless circulated previously, in which case nominations received should be included)

vi. what accounts or reports are to be presented

vii. the content of resolutions received by the secretary or proposed by the board.

viii. Advice on how to obtain information specified in 40. (b)

ix: A statement setting out the right, and detailing the procedure by which a Member may appoint a proxy in their place.

**19. Proxies:**

a) A Member who is absent or who knows in advance they will be absent from a general meeting, may appoint any Person to act as their proxy, provided that the individual chosen to act as their proxy does not already represent five or more members as their proxy.

b) Proxies may only validly be appointed by a notice in Writing which:

i: States the name and Address of the Member appointing the proxy;
 ii: Identifies the Person appointed to be that Member&#39;s proxy and the general meeting in relation to which that Person is appointed;
 iii: Is signed by or on behalf of the Member appointing the proxy, or is authenticated in such manner as the Directors may determine; iv: Is delivered to the Co-operative in accordance with the Articles and any instructions contained in the notice of the general meeting to which they relate.

c) The Cooperative may require proxy notices to be delivered in a particular form and may specify different forms for different purposes, such procedures must in order to be enforceable be specified in the standing orders of the cooperative.

d) Proxy notices may specify how the proxy appointed under them is to vote (or that the proxy is to abstain from voting) on one or more of the resolutions, otherwise the proxy notice shall be treated as allowing the Person appointed the discretion as how to vote on any matter.

e) A Person who is entitled to attend, speak or vote (either on a show of hands or a poll) at a general meeting remains so entitled in respect of that meeting or any adjournment of the general meeting to which it relates.

f) An appointment using a proxy notice may be revoked by delivering to the Cooperative a notice in Writing given by or on behalf of the Person by whom or on whose behalf the proxy notice was given and authenticated though signature or through other means (when such means have been detailed though the standing orders of the cooperation). A notice revoking a proxy appointment only takes effect if it is delivered before the start of the meeting or the adjourned meeting to which it relates.

g) If a proxy notice is not signed by the Person appointing the proxy, it must be accompanied by evidence in Writing that the person signing it has the authority to execute it on the appointor&#39;s behalf.

**20: Chairing Meetings**

a) At the beginning of each General Meeting, the Members present shall appoint though either consensus or popular vote one of their number to be the chairperson for that meeting. The appointment of a chairperson shall be the first item of business at the general meeting.

**21. Attendance and Speaking at General Meetings**

a) A Member is able to exercise the right to speak at a general meeting and is deemed to be in attendance when that Person is in a position to communicate to all those attending the meeting. The Directors may make whatever arrangements they consider appropriate to enable those attending a general meeting to exercise their rights to speak or vote at it including by Electronic Means. In determining attendance at a general meeting, it is immaterial whether any two or more Members attending are in the same place as each other.

b) The chairperson of the meeting may permit other persons who are not Members of the Cooperative to attend and speak at general meetings, without granting any voting rights.

**22. Adjournment**

a) If a quorum is not present within half an hour of the time the general meeting was due to commence, or if during a meeting a quorum ceases to be present, the chairperson must adjourn the meeting.

b) The chairperson of a general meeting may adjourn the meeting whilst a quorum is present if:

i:. The meeting consents to that adjournment; or
 ii:. It appears to the chairperson that an adjournment is necessary to protect the safety of any persons attending the meeting or to ensure that the business of the meeting is conducted in an orderly manner.

c) The chairperson must adjourn the meeting if directed to do so by the meeting.

d) When adjourning a meeting the chairperson must specify the date, time and place to which it will stand adjourned or that the meeting is to continue at a date, time and place to be fixed by the Directors.

e) If the meeting is adjourned for 14 days or more, notice of the adjourned meeting shall be given in the same manner and fashion as the notice of the original meeting.

f) No business shall be transacted at an adjourned meeting other than business which could not properly have been transacted at the meeting if the adjournment had not taken place.

**23. Resolutions at general meetings:**

a) All members have the right to propose resolutions. If a resolution is proposed that includes any of the actions listed in 23. (c) and (d) below, its content must be provided to the secretary in time for inclusion in the notice of the general meeting. Otherwise resolutions may be proposed in the course of the meeting, in accordance with any standing orders that are in force.

b) Votes will be conducted on a show of hands, and the secretary shall declare a provisional result on the basis of whether they judge that the vote would pass if it was counted; at any point during the general meeting, any member may request that the result of any vote be verified with a count. When the meeting is closed, any provisional result is recorded in the minutes as final.

c) Resolutions require a clear majority of the vote at the general meeting to be in favour to pass, except the following resolutions require more than 75% support:

i. resolutions to change the conditions attached to membership;

ii. amendments to these Articles;

iii. resolutions to wind up or dissolve the company (on which only user members may vote);

iv. expulsion of a member in accordance with 10. above;

v. dissolution of the Commonwealth Council.

d) The following proposals are special resolutions that must be passed and confirmed with more than 75% support and in accordance with the law:

i. amalgamation with, or transfer of engagements to, another corporate body.

ii. a resolution for winding up.

e) Certain proposals may, even if they fail to achieve a majority, nonetheless constitute a petition that the Company must comply with if they achieve the support of one third of the voting strength in a general meeting. These are:

i. the board to publish its policies regarding social investment of liquid assets, equal opportunities, health and safety and/or corporate social responsibility;

ii. the board to begin the process of convening a commonwealth council, in accordance with rule 43. (b)

f) A person who is not a member of the Company shall not have any right to vote at a general meeting of the Company; but this is without prejudice to any right to vote on a resolution affecting the rights attached to a class of the Company&#39;s debentures.

**24. Voting by classes:**

a) If any member requests that a vote be counted rather than taken on a show of hands, the votes cast by each class will be weighted (that is, treated as being a greater or lesser amount) by the secretary.

**25. Written Resolutions**

a) Resolutions may be passed at general meetings or by written resolution.

b) A written resolution passed by Members shall be effective if it has been passed in accordance with the requirements of the Act which includes sending a copy of the proposed resolution to every Member. Written resolutions may comprise several copies to which one or more Members have signified their agreement.

c) A written resolution shall be deemed to have been passed if, within 28 days of the written resolution&#39;s circulation date:

i) Written approval has been received from at least 75% of the Members where the resolution is a special resolution;
 ii) Written approval has been received from at least 51% of the Members where the resolution is an ordinary resolution.

d) In accordance with the Companies Acts, resolutions to remove a Director or auditor (or their equivalent) of the Cooperative before the end of their period of office shall not be passed by written resolution.

**26. Board of directors**

a) The number of directors on the board (counting only those with voting rights) must be no more than twelve, and no less than three. The board may co-opt new directors to fill vacancies at any time. Any directors that have been co-opted to the board will resign or stand for election at the next available general meeting.

b) Questions arising at a Directors&#39; meeting shall be decided by a majority of votes.

c) No nonuser members may be elected or co-opted to the board.

d) Standing orders may allow the holders of up to three posts in the Company to automatically join the board as non-voting Executive Directors.

e) For so long as the number of user members is less than ten, unless a general meeting resolves to hold elections all user members will be automatically appointed to the board (though they may decline to accept the appointment) and 26. (f) will not apply.

f) The founders of the Company will be the first directors. At all subsequent annual general meetings all directors will resign so that those wishing to continue must seek re-election.

g) Resolutions to elect each candidate will be voted on in turn until there are no vacancies remaining. The order in which candidates are voted on may be determined by standing orders; if not, candidates that have served longest on the board will be first. If some candidates have served for an equal length of time, those that have been members longest will be first.

h) Larger and more complex co-operatives may require directors with suitable skills and experience. Standing orders may allow the board to endorse or recommend some candidates but not others for election (provided the number of endorsements is either more, or less, than the number of vacancies); if that is the case, the board must convene a Commonwealth Council to supervise the process of qualification and nomination, rather than choose its own composition. The board can always report objectively on the relevant qualifications of candidates, or on the extent of candidates&#39; participation in any preparatory process that was open to all members to take part in.

i) All members have the right to stand as candidates, subject to any reasonable nomination procedure specified in standing orders. No one can serve on the board if they:

i. have resigned in writing to the secretary;

ii. are not a member, or the nominated representative of a member organisation;

iii. are removed from office by a resolution passed by a general meeting;

iv. fail to attend three consecutive meetings without adequate explanation;

v. commit fraud, offences involving harassment, antisocial behaviour or violence, or any act of financial impropriety, or failed to disclose when standing for election any previous offence of this type, or contravene rule 37.; or

vi. are an undischarged bankrupt, or otherwise prohibited by law from acting as a director of a company; or

vii. they are subject to restrictions provided for in 26. (m) below.

j) The board may also appoint other officers in addition to the Secretary and Treasurer, and form subcommittees, as it sees fit, and in accordance with any standing orders. Officers shall have the powers and duties specified by law and by the board, and may be removed by the board. The board may also convene consultative committees composed of members of the co-operative, which may be elected or unelected, for any purpose.

k) Directors and officers may be reimbursed for any expenses incurred in the course of carrying out their duties. Directors may receive an attendance allowance and may be paid wages, but only for services actually performed for the co-operative, and in accordance with a pay policy submitted to the next available general meeting for approval.

l) A director can be removed from office by a resolution passed by a two thirds of the other directors present and voting at a duly convened board meeting if that director:

i. is in breach of one or more of the co-operative&#39;s published Articles, policies or procedures, or a contract with the co-operative, or their actions may amount to gross misconduct; and

ii. has been given at least one week&#39;s notice of the resolution to remove; and

iii. has failed to fully remedy any breach (or has no possibility of doing so); and

iv. has had the opportunity to respond to all board members with an explanation or with undertakings.

m) A vote to remove a director in accordance with 26. (l) may include the following provisions:

i. a suspension of the removal subject to binding undertakings; with the removal to take immediate effect in the event that those conditions are not met.

ii. a ban from the board for a period of up to two years.

n) a claim that the actions of the director met the standard of gross misconduct, in which case they will be suspended as a director on receipt of the resolution, and remain so until the vote is held

**27. Quorum**

a) No business will be transacted at any general meeting unless a quorum is present. A quorum is present if one of the following conditions is fulfilled (the condition used must represent more of the membership then the other for the Quorum to be present)

i. there are three or more user members present either in person or represented by proxy.or

Ii. 50% of the membership either in person or represented by proxy

b) A quorum at a board meeting is three voting directors, or two thirds (rounded down) of the total number of voting directors, whichever is greater.

c) If standing orders permit, a member may be considered present if they are participating through a live electronic link or other means of communication, as the standing orders permit.

**28. Member control**

a) No amendment may be made to these Articles that would allow non-members to have more than 25% of total voting strength at any general meeting, or would remove this clause.

# **Section Five**

# **Application of Profits**

29. The Company has the general aims of creating common wealth, building an indivisible reserve and providing a return on investment no more than is necessary to attract and retain the capital it requires.

30. The Company may borrow up to a maximum of £10,000,000 and may issue debt securities provided that this does not amount to receiving money on deposit. Any interest paid on share capital held by user members, or on funds borrowed from user members will not exceed the highest rate for fixed term business lending published by ICOF Ltd (company no. 01109141). In keeping with co-operative accounting practice, interest on share capital is considered a business expense and is not included in profits.

**31. Full consideration**

a) The Company shall not transfer any of its assets other than for full consideration. Provided the conditions in article 31 (b) are satisfied, article 31 (a) shall not apply to:

i. the transfer of assets to any specified asset-locked body, or (with the consent of the Regulator) to any other asset-locked body; and

ii. the transfer of assets made for the benefit of the community other than by way of a transfer of assets into an asset-locked body.

iii. paying bonuses, incentives and other rewards to members and employees, provided that this is in pursuit of the Company&#39;s objects in 4.

b) The conditions are that the transfer of assets must comply with any restrictions on the transfer of assets for less than full consideration which may be set out elsewhere in the Memorandum and Articles of the Company.

**32. Application of annual profits:**

a) Profits of the Company will be applied as follows, subject to approval at the AGM:

i. to form a general reserve for the continuation and development of the company;

ii. to make payments in accordance with the Cooperative Principle of concern for community, for social, co-operative and community purposes, of benefit to the community.

**33. Division on dissolution:**

a) In the event of the winding up or dissolution of the Company the assets of the Company will first, according to law, be used to satisfy its debts and liabilities.

b) The remaining assets will be transferred to a common ownership enterprise that has objects consistent with the objects of the Company stated in 4. above, subject to any restrictions in 31. above, as may be nominated by the members at the time of or prior to the dissolution. If no such organisation is nominated, the assets will be transferred to

…………………………………………………… [by default: The Co-operative College (charity number 1060008)]

c) In the event that for whatever reason any residual assets cannot be transferred as described above, they will be given for charitable purposes. The Regulators consent will be sought before any such transfer is made. No amendment will be made that would reduce the amount given to social and charitable purposes, or remove this sentence.

# **Section Six**

# **Autonomy**

34. The Company has the general aims of maintaining its autonomy and independence and empowering members and other stakeholders at the most local level possible.

**35. Limitation on powers:** For the avoidance of doubt the Company shall not engage in any activity by virtue of any of these Articles that would require a permission from the Prudential Regulation Authority or Financial Conduct Authority (or any body that succeeds their functions) to carry on that activity without first having applied for and obtained such permission.

**36. Financial and contractual autonomy**

a) Debt securities that would result in one natural person holding more than a quarter of all investments in the Company (or that would exceed the limits specified in 1.6a), require the board to agree measures that will ensure that the lender (or their assigns) cannot unduly influence the future management of the Company.

b) The board of directors will regularly review all contracts and undertakings to ensure that the Company continues to be controlled by its membership.

**37. Outside interests on the board of directors**

a) All directors will, on taking office, indicate in a register of interests any material interests they have, or positions that they hold in other organisations, that might cause conflict with the objects of the Company.

b) All directors will declare any such conflict of interest that they have in respect of any business before the board and will withdraw from votes in respect of that matter unless;

i. so many directors are conflicted on an issue that the board would not be quorate, or

ii. the Commonwealth Council determines that the interest will not lead to significant conflict.

**38. Subsidiarity**

a) If the board is satisfied that a subset of members in one or more classes have a relationship with the Company that is distinct from that of other members, for example by living in a relatively isolated location, enjoying the benefits or impacts of some of the Company&#39;s activities and not others, or having distinctive requirements for communication with the Company, or in other ways, then it will implement an action plan to ensure that decision-making that disproportionately affects those members is more accessible and accountable to them.

b) In the event of a petition by five members or 15% of those identified as having a distinctive interest (whichever is greater), the board must convene a consultative meeting of all such members; and either publish the action plan referred to in 31. a or seek independent and qualified advice as to whether a plan is needed.

c) If an interest group meeting resolves by a majority vote that a current or proposed action plan is inadequate, then it can empower up to three named members of the Company to put an alternative proposal to the next general meeting (for example, convening a Commonwealth Council with interest group representation).

**39. Key decisions:** The board of directors, on behalf of the Company, may carry on any activity, that is within the law and in the opinion of the board, may benefit the Company&#39;s objects, However, the following issues are designated &#39;key decisions&#39;:

i. the sale, transfer or disposal of assets worth in excess of £100,000 to the same buyer;

ii. any conflicted decision relying on rule 30. b above;

iii. a mortgage or charge on non-residential property that would have the effect of increasing the debt/equity ratio of the Company to a level in excess of 4:1;

iv. issuing debt securities to private individuals, or with the option to transfer, with a value of more than £50,000 v. investing in corporate bodies in which the Company will have more than 50% of the value or voting strength of their share capital;

vi. remuneration of employees, where the pay differential within the Company exceeds 4:1;

vii. remuneration of directors, and selection of candidates for the board;

viii. compulsory redundancies;

ix. the level of pay for workers who are not voluntary, but will not be legally entitled to the highest rate of statutory minimum wage (including outsourcing of work previously carried out by employees);

x. expenditure on legal advice with the purpose of avoiding compliance with statutory regulation or with contractual and legal obligations to other co-operatives;

xi. Entering into contractual relationships with entities, individuals or organisations which could be considered commercial or which otherwise will bind the cooperative or members of it, into working on projects, initiatives or other tasks.

xii. Entering into or otherwise engaging in the contracting of loans and debt securities or the investment of funds when the amount for consideration for either party is above five hundred pounds.

b) Key decisions can be authorised in the following ways:

i. by a resolution at a general meeting;

ii. If a Commonwealth Council (see 43.- 44. below) has been convened, key decisions must be brought to its attention no less than two weeks before they come into effect; and if a majority of the Commonwealth Council request further consultation, the proposal may not be enacted until the board and the Commonwealth Council both have a majority in favour;

iii. If a Commonwealth Council has not been convened, or at its request, notice of the key decision will be brought to the attention of the membership by means of a public notice, electronic communication or other communication likely to be received by most members in sufficient time for a general meeting to be called or attended by the members (in accordance with 18. (c) ) before the decision comes into effect;

iv. If the board does not include two members from each of two member classes, or in any case for decisions 39. ii or vii, then paragraph 39. (b) iii above cannot be applied, and either 39. (b) i or 39. (b) ii must be used instead. All authorisations will be recorded and reported to the next AGM (see 40. (b) ).

## **Section Eight**

## **Education and information**

**40. Provision of information**

a) A copy of these Articles, and any amendments made to them, will be given free of charge to every member on admission or on request. The board will accommodate any reasonable request to explain or clarify the meaning of the Articles, and justify its interpretation of them.

b) The following information will be advertised in the notice of all general meetings, with advice on how full copies may be obtained promptly and without charge:

i. standing orders relating to the meeting in question;

ii. a list of key decisions taken or consulted on since the last AGM, and how they were resolved;

iii. a guide to the rights and responsibilities of directors and the process for members to put themselves forward for election;

iv. details of other ways for members to participate in the governance of the Company;

v. a list of policy documents covering standards and procedures that apply to the full range of the Company&#39;s activities, including any policies required by 23. (e) i;

vi. the most recent action plans or reports prepared in the last three years in line with 38. (b)

c) The following information will be recorded, retained and made available at no charge to members:

i. agendas, minutes and papers presented to general meetings;

ii. quarterly management accounts (unless the annual turnover of the Company is below £25,000);

iii. annual returns submitted to the FCA, HMRC and federal bodies;

iv. responses to statutory consultations made by the Company;

v. job descriptions and line management of any staff employed, and statistics relating to staff disputes and grievances, workplace injuries and staff retention.

d) No information will be provided to a member or any other person, or made available for general viewing, that would disclose details of the financial transactions of another member with the Company, other than with their permission. If the board refuses a request for information, it must explain what reason it has for withholding the information.

e) The board may redact portions of documents provided to members for reasons of commercial confidentiality or personal privacy provided it is clear and where this has been done.

**41. Presentation of accounts and reports**

a) At each AGM the following reports must be presented:

i. Accounts for the previous financial period;

ii. A social impact report for the previous financial period (see 46.).

b) The following may be included in the above reports, but if published separately should also be available for members to discuss and vote on:

i. A report from the Commonwealth Council, if it is convened (see 43.);

c) During each financial year, the Company will normally appoint a person qualified to the standard required by law who is neither a member nor an employee of the Company to either audit the Company&#39;s accounts and balance sheet for the year or prepare an independent accountants report; and a social auditor if required to do so by 47. (b) below.

d) The appointments will be confirmed at the next available general meeting, and at every annual general meeting subsequently unless and until the obligation is removed.

e) If the reports in 41.(a) are not presented to an annual general meeting, or fail to win majority approval, another general meeting must be called within 28 days and directors must take all reasonable steps to ensure that reports are presented to that meeting that the membership can support. If other reports, any report from the Commonwealth Council (see rule 43.), a report of consultations on any key decisions (see rule 39.), and the social impact report (see rule 46.) are not accepted, the board (with the Commonwealth Council a when appropriate) must publish an action plan to secure member support for future reports within two months; if any report is rejected in a second consecutive year, then a general meeting must be called within four months to consider an improved version.

42. Annual Return: Every year, and in accordance with the requirements of the law, the Secretary will send the annual return relating to the Company&#39;s affairs for the required period to the Regulator.

## **Section Eight**

## **The Commonwealth Council**

**43. The Commonwealth Council**

a) is an oversight body that does not operate immediately following incorporation, but which can be activated at a later date, for example if the Company has become larger and more complex; plays an important role in the co-operative sector locally; is facing divisive or controversial decisions; or wishes to offer an additional voice to minority groups or classes within the membership.

b) The Commonwealth Council may be convened by the board of directors at any time, or by a resolution of the members at a general meeting, or following a petition of the members in accordance with clause 23. (e) ii. In the absence of a Commonwealth Council, functions that it might have carried out will remain the responsibility of the board. [Rules 43. (b – f) do not apply unless enabled in accordance with 43. (a)]

c) The Commonwealth council will be free to consider any matter affecting the Company, may publish its views on any matter, and may summon any employee or officer of the Company to attend their meetings and answer questions relating to the business of the Company. It can be dissolved only by a resolution at a General Meeting carried with more than 75% of votes.

d) The size and procedures of the Commonwealth Council will be determined by its standing orders, which will be prepared by the board of directors (other than matters specified by a general meeting). It must have no fewer than four members.

e) If the Commonwealth Council has responsibility for qualification and nomination of directors in accordance with 26. (h), it will (in consultation with the board) establish objective requirements for qualification that can reasonably be met by at least some existing user members; and seek sufficient nominees meeting these criteria to ensure a contested election.

f) Invitations to apply for membership will normally be publicised among the following stakeholders, unless they are specifically excluded by the board of directors:

i. Those eligible for membership under 7. and 8.;

ii. Employees, volunteers, service users, local residents, significant suppliers and customers, and investors, that would not normally be eligible for membership;

iii. Anyone who was previously a member of the Company within the last five years; and

v. Any co-operative that may be considered relevant due to geography, similarity, common membership or trading relationship;

g) If at any time following such invitations fewer than eight people express willingness to serve on the Commonwealth Council, or if all but three candidates come from a single one of the categories listed in 43. (f), the board may suspend it until the next General Meeting due to lack of interest. Applications for membership will be considered according to the standing orders in force, subject to the requirements that;

i. the process of selection for membership of the Commonwealth Council should be impartial, fair, transparent, and non-discriminatory; and

ii. members of any one of the four categories listed in 43. (f) should not comprise more than 75% of the Council, and no more than one person may sit on both Council and Board.

h) Members of the Company engaged in a dispute relating to the Company may request the Commonwealth Council to mediate between them and shall do so before elevating any such dispute to the board or to the membership as a whole.

**44. Disputes and conflicts**

a) Standing orders, or other procedures that have been adopted, may provide for members to refer disputes to the Commonwealth Council as part of grievance, disciplinary or complaints procedures (either as first recourse, or as part of an appeals process). If there is any evidence of a criminal act the police should be fully informed before any other action is taken.

b) If the dispute cannot be resolved according to standing orders, by the Commonwealth Council or the board, whether because

i. one or both parties to the conflict sit on the body that would normally be responsible;

ii. the body in question does not feel it has the capability to manage the conflict;

iii. it is not covered by existing procedures, or concerns their adequacy;

iv. it is argued that it is a decision by the body in question that has created the conflict;

then the parties should promptly agree to seek independent support from others in the co-operative movement locally, respected figures in the local community or from relevant federal bodies. If this cannot be done, an accredited mediator or arbitrator should be contracted.

## **Section Nine**

## **Sustainable Development**

45. The Company has the general aim of evaluating its impact on the community and the environment in which it operates, and developing policies that reduce harmful impacts and increase positive impacts.

**46. Social impacts**

a) The board of directors is responsible for preparing an annual social impact report. These will consist of quantitative and qualitative data for a set of indicators selected by the social impact reporting panel; which in turn should relate clearly to the objects in 4. above.

b) Indicators should cover, and clearly distinguish between, outputs (what the Company has done), outcomes (what the direct effects of this have been) and impacts (what indirect changes can be reasonably attributed to the Company&#39;s activities in this and previous years).

c) The indicators should allow where possible for comparison with other accounting periods. If an indicator used in a previous accounting period is to be withdrawn, the reason for its withdrawal should be stated alongside a final measurement.

d) The report should identify areas where the findings suggest that performance can be improved, and may include directions to the board to develop new plans and policies addressing these areas, adopt or withdraw indicators (including suggested methods of data capture), review the aims and objects with a view to amendment, and/or report to members on progress. The report will not direct the board in ways that would otherwise impact on resource allocation or workloads.

**47. Social impact reporting panel**

a) At least one month before being presented to the annual general meeting, the social impact report will be prepared or reviewed by a social accounting panel which will consider

i. to what extent the report is comprehensive;

ii. whether the information gathered is reliable and reasonably interpreted;

iii. whether the aspirations of members for steady improvements in social impact are sufficiently addressed;

iv. whether consequential actions have been identified and specified; before deciding whether to commend the report to the meeting, or refer it back for improvement.

b) The panel should normally be chaired by an independent, qualified social auditor. However, a general meeting may choose to accept a current or subsequent year&#39;s report approved by a panel chaired by a member of the Company provided that they have not served as a board member in the previous two years (this requirement is waived if more than half the members are also board members).

c) The panel will be made up of members of the Company approved by the board and commonwealth council (if it is active).

Signatures of founder members: Full names in block capitals:

Secretary:
